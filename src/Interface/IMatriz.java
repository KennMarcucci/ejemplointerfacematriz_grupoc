/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

/**
 * Definición de métodos  (Contratos de uso)--> Forma como todos deben especificar y llamar 
 * el método
 * @author madarme
 */
public interface IMatriz {
    
    /**
     * Método obtiene la suma de todos los elementos de la matriz
     * @return un entero con la suma de la matriz
     */
    public int getSumaTotal();
   
    
}
